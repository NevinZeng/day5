## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

1. Conduct Code Review and Show Case to learn excellent code.

2. Do TDD exercises and complete four stories.

3. Discuss the details of teamwork and the way of division of labor.

## R (Reflective): Please use one word to express your feelings about today's class.

    Enough.

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

    There is a lot of code, it needs to be clear and organized, and sometimes it gets caught in the trap of details.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

    I have a deeper understanding of TDD, and it is helpful for me to write test samples in software development.
